#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

helper = '''
uso: python vetor.py [argumentos]

onde os argumentos são:
    -h     : Exibe esta mensagem de ajuda.
     i n   : Insere o número 'n' na posição 'i' do vetor.
    -i     : Remove o elemento de índice 'i' do vetor.
     f n   : Busca o primeiro 'n' no vetor e imprime 'v[i] -> n'.
     min   : Busca o menor número no vetor e imprime 'v[i] -> n'.
     max   : Busca o maior número no vetor e imprime 'v[i] -> n'.
     print : Imprime o vetor.
'''

class Vetor(object):

    def __init__(self, length):
        self.used = 0
        self.data = [0 for i in range(length + 1)]

    @property         
    def length(self):
        return len(self.data) - 1
        
        
    def is_empty(self):
        return self.used == 0
        
                   
    def is_full(self):
        return self.used == self.length
        
        
    def insert_at(self, index, value):
        index = min(self.used, index)
        
        if not self.is_full():
            for i in range(self.used, index, -1):
                self.data[i] = self.data[i - 1]

            self.data[index] = value
            self.used += 1


    def remove_at(self, index):
        if not self.is_empty() and index < self.used:
            for i in range(index, self.used):
                self.data[i] = self.data[i + 1]
            
            self.used -= 1
            

    def find(self, value):
        for i in range(self.used):
            if self.data[i] == value:
                return i
        
        return -1
        
        
    def max(self):
        if not self.is_empty():
            value = self.data[0]
            
            for i in range(1, self.used):
                value = max(value, self.data[i])
            
            return value
    
    
    def min(self):
        if not self.is_empty():
            value = self.data[0]

            for i in range(1, self.used):
                value = min(value, self.data[i])

            return value
    

    def print_value_at(self, index):
        if index >= 0 and index < self.used:
            print 'v[{}] -> {}'.format(index, str(self.data[index]))


    def __str__(self):
        return ', '.join([str(self.data[i]) for i in range(self.used)])


def main():
    vetor = Vetor(50)
    index = 1
    
    while (index < len(sys.argv)):
        
        try:
            i = int(sys.argv[index])

            if sys.argv[index][0] == '-':
                vetor.remove_at(-i)
                
            else:
                index += 1
                n = int(sys.argv[index])

                vetor.insert_at(i, n)
        
        except:
            arg = sys.argv[index].lower()
            
            if arg == 'print':
                print vetor
                
            elif arg == 'max':
                i = vetor.find(vetor.max())
                
                if i is not None: vetor.print_value_at(i)
                
            elif arg == 'min':
                i = vetor.find(vetor.min())
                
                if i is not None: vetor.print_value_at(i)
                
            elif arg == 'f':
                index += 1
                n = int(sys.argv[index])
                
                i = vetor.find(n)
                
                if i is not None: vetor.print_value_at(i)
                
            elif arg == '-h':
                print helper
                
        index += 1


if __name__ == '__main__':
    main()
